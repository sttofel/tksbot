exports.run = async (client, message, args) =>
{
    if (!message.member.hasPermission("MENAGE_MESSAGES")) return message.reply("oof.");
    if (!args[0]) return message.channel.send("oof");

    message.channel.bulkDelete(args[0]).then(()=>{
        if (args[0]==1) {
            message.channel.send(`:fire: Foi removido ${args[0]} mensagem :fire:`).then(msg => msg.delete(5000)); 
        }else{
            message.channel.send(`:fire:  Foram removidas ${args[0]} mensagens :fire:`).then(msg => msg.delete(5000)); 
        }
    });
}