// Discord
const Discord = require('discord.js');
const client = new Discord.Client();
const config = require("./config.json");

// Mysql
const mysql = require('mysql');
const db = new mysql.createConnection({
    host: '127.0.0.1',
    port: '3306',
    user: 'root',
    password: 'root',
    database: 'bot'
});


client.prefix = config.prefix;

client.on("ready", () =>{
    console.log("Bot iniciado!");
    console.log("\nEste bot esta em " + client.guilds.size + " servidores");
    client.user.setActivity("Servidores: " + client.guilds.size, {type: "Watching"});
});

db.connect(err =>{
    if(err) return console.log(err);
    console.log('Conectado com sucesso ao banco de dados!');
});

//joined a server
client.on("guildCreate", guild => {
    let servername = guild.name;
    let serverid = guild.id;

    let insert = `INSERT INTO servidores(servidor_id, servidor_nome) VALUES(${serverid},"${servername}");`;

    let select = `SELECT * FROM servidores where servidor_id = ${serverid};`;

    let query = db.query(select, function(err, result) {
        if (err) throw err;

        if (result.length==0) {
            db.query(insert);
            console.log("O bot entrou no servidor " + servername + " e foi adicionado no banco de dados com sucesso!");
            client.user.setActivity("Servidores: " + client.guilds.size, {type: "Watching"});
        }
    });

});

client.on("guildDelete", guild => {
    let servername = guild.name;
    let serverid = guild.id;

    let del = `DELETE FROM SERVIDORES WHERE servidor_id = (${serverid});`;

    let select = `SELECT * FROM servidores where servidor_id = ${serverid};`;
    
    let query = db.query(del, function(err, result) {
        if (err) throw err;

        if (result.length==1) {
            db.query(insert);
            console.log("O bot foi removido do servidor " + servername + " e foi deletado do banco de dados com sucesso!");
            client.user.setActivity("Servidores: " + client.guilds.size, {type: "Watching"});
        }
    });

});


// Adicionar e remover cargos
client.on('raw', async dados => {
    if(dados.t !== "MESSAGE_REACTION_ADD" && dados.t !== "MESSAGE_REACTION_REMOVE") return;
    if(dados.d.message_id !== "606906727522369539") return;

    let servidor = client.guilds.get("606830630042468372");
    let membro = servidor.members.get(dados.d.user_id);

    let suporte = servidor.roles.get('606831229542858752');
    let vendas = servidor.roles.get('606908698358710322');

    if (dados.t === "MESSAGE_REACTION_ADD") {
        if (dados.d.emoji.id === "606905594435076099") {
            if (membro.roles.has(vendas)) return;
            membro.addRole(vendas);            
        } else if (dados.d.emoji.id === "606905594443726878") {
            if (membro.roles.has(suporte)) return;
            membro.addRole(suporte);            
        }
        
    }

    if (dados.t === "MESSAGE_REACTION_REMOVE") {
        if (dados.d.emoji.id === "606905594435076099") {
            if (membro.roles.has(vendas)) return;
            membro.removeRole(vendas);            
        } else if (dados.d.emoji.id === "606905594443726878") {
            if (membro.roles.has(suporte)) return;
            membro.removeRole(suporte);            
        }
        
    }
});

// Comandos
client.on("message", async message =>{
    let msg = message.content.toLowerCase();
    if (message.author.bot) return underfined;
    if (message.content.indexOf(client.prefix) !== 0) return;
    const args = message.content.slice(client.prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();
    try {
        let commands = require(`./commands/${command}.js`);
        commands.run(client, message, args);        
    } catch (e) {
        console.log(e);        
    } finally{}
});


const evtFiles = readdirSync('./events/')
console.log('log', `Carregando o total de ${evtFiles.length} eventos`)
evtFiles.forEach(f => {
  const eventName = f.split('.')[0]
  const event = require(`./events/${f}`)

  client.on(eventName, event.bind(null, client))
})

client.on('error', (err) => {
  console.log('error', err)
})

client.login(config.token);